# Star Wars Resistence API

## Functionalidades
- [X] Cadastrar rebelde
- [X] Reportar localização
- [X] Reportar traidor
- [X] Realizar negociação
- [X] Obter relatórios

## Tecnologias utilizadas
* Java
* Spring boot
* Spring Data 
* Hibernate
* PostgreSQL
* Maven

## Como instalar

#### Dependências
* JDK 1.8
* PostgreSql: v9.6;

#### Instalação
1. Criar banco de dados vazio com o nome **swr**
2. Editar configurações no arquivo *src/main/resources/application.properties*
3. Abrir terminal no diretório raiz do projeto
4. Executar o comando:
    ```
    > mvnw spring-boot:run
    ```

5. A API deve estar disponível na porta **9000**.

## Como utilizar

#### Padrão de retorno da API
```
{
    "dados": {...} ou [...],
    "mensagem": "Descrição sobre o retorno",
    "sucesso": true ou false
}
```

* **Adicionar rebeldes**
    * **POST** - /api/rebeldes
    * > Exemplo de corpo da requisição:
    ```
    {
        "nome":"NOME_DO_REBELDE",
        "idade":IDADE_DO_REBELDE,
        "genero":"Masculino / Feminino",
        "localizacao": {
            "nome":"NOME_DO_LOCAL",
            "latitude":77.77,
            "longitude":-88.88
        },
        "inventario": [
            {
                "quantidade":1,
                "tipo": {
                "nome":"Munição"
            }
            },
            {
                "quantidade":5,
                "tipo": {
                    "nome":"Comida"
                }
            }
        ]
    }
    ```
    * > A resposta deve ser o novo objeto rebelde criado

* **Reportar localização**
    * **PATCH** - /api/rebeldes/**{rebelde_id}**
    * > Exemplo de corpo da requisição:
    ```
    {
        "nome":"Sirius",
        "latitude":34.37,
        "longitude":-11.28
    }
    ```
    * > A resposta deve ser o objeto rebelde com a localização atualizada.

* **Reportar traidor**
    * **PATCH** - /api/rebeldes/**{rebelde_id}**/traicao
    * > O corpo da requisição deve ir vazio.
    * > A resposta deve ser o objeto rebelde com o número de reportes atualizado.

* **Realizar negociação**
    * **PATCH** - /api/rebeldes/**{rebelde_id1}**/negociacao/**{rebelde_id2}**
    * > Exemplo de corpo da requisição:
    ```
    {
        "oferta1":[
            {
                "quantidade":1,
                "tipo":{
                    "nome":"Munição"
                }
            },
            {
                "quantidade":1,
                "tipo":{
                    "nome":"Comida"
                }
            }
        ],
        "oferta2":[
            {
                "quantidade":1,
                "tipo": {
                    "nome":"Arma"
                }
            }
        ]
    }
    ```
    * > A resposta deve ser os dois objetos rebeldes com o respectivos inventários atualizados.

* **Obter relatórios**
    * **GET** - /api/rebeldes/relatorio
    * > A resposta deve possuir as seguintes propriedades:
        * **mediaItensPorTipo** : Lista dos tipos de item cadastrados no sistema com sua quantidade média por rebelde. Exemplo:
        ```
        {
            "nome": "Arma",
            "quantidadeMediaPorRebelde": 4
        }
        ```
        * **pontosPerdidosDevidoATraidores** : Valor contendo a quantidade de pontos dos itens presos com traidores.
        
        * **porcentagemTraidores** : Valor contendo a porcentagem de rebeldes que são traidores.

        * **porcentagemRebeldes** : Valor contendo a porcentagem de rebeldes que **não** são traidores.



