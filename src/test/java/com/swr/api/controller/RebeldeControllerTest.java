package com.swr.api.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.util.Optional;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.swr.api.model.Item;
import com.swr.api.model.Localizacao;
import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;
import com.swr.api.repository.IItemRepository;
import com.swr.api.repository.ILocalizacaoRepository;
import com.swr.api.repository.IRebeldeRepository;
import com.swr.api.repository.ITipoItemRepository;
import com.swr.api.seeder.RebeldeSeeder;
import com.swr.api.util.Mensagem;
import com.swr.api.util.RequestNegociacao;
import com.swr.api.util.UtilTests;

public class RebeldeControllerTest extends UtilTests {
	private String baseControllerUrl = "/api/rebeldes";

	@Autowired
	private RebeldeSeeder rebeldeSeed;

	@Autowired
	private IRebeldeRepository rebeldeRepository;

	@Autowired
	private ITipoItemRepository tipoItemRepository;

	@Autowired
	private IItemRepository itemRepository;

	@Autowired
	private ILocalizacaoRepository localizacaoRepository;

	private Rebelde rebeldeTest;
	private Rebelde rebeldeTest2;
	private Rebelde rebeldeTest3;
	private Rebelde rebeldeTraidor;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		this.limparTabelas();
		this.rebeldeTest = rebeldeSeed.inserirRebeldeTeste3();
		this.rebeldeTest2 = rebeldeSeed.inserirRebeldeTeste2();
		this.rebeldeTest3 = rebeldeSeed.inserirRebeldeTeste1();
		this.rebeldeTraidor = rebeldeSeed.inserirRebeldeTesteTraidor();
	}

	@Test
	public void cadastrarRebelde() throws Exception {
		String jsonObject = json(this.rebeldeTest3);
		
		this.mockMvc.perform(post(baseControllerUrl)
				.contentType(contentType).content(jsonObject))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(true)))
				.andExpect(jsonPath("$.dados.nome", equalTo(this.rebeldeTest3.getNome())))
				.andExpect(jsonPath("$.dados.reportes", equalTo(0)))
				.andExpect(jsonPath("$.dados.localizacao.nome", equalTo(this.rebeldeTest3.getLocalizacao().getNome())))
				.andExpect(jsonPath("$.dados.inventario.*", hasSize(2)))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.RECURSO_CRIADO.get())));
	}

	@Test
	public void atualizarLocalizacao() throws Exception {
		Rebelde rebeldeToUpdate = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTest2.getNome());

		Localizacao novaLocalizacao =
				new Localizacao("Miller", new BigDecimal("-97.99"), new BigDecimal("11.43"));

		String jsonObject = json(novaLocalizacao);

		this.mockMvc.perform(patch(baseControllerUrl + "/" + rebeldeToUpdate.getId())
				.contentType(contentType).content(jsonObject))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(true)))
				.andExpect(jsonPath("$.dados.nome", equalTo(rebeldeToUpdate.getNome())))
				.andExpect(jsonPath("$.dados.localizacao.nome", equalTo(novaLocalizacao.getNome())))
				.andExpect(jsonPath("$.dados.localizacao.latitude", 
						equalTo(Double.parseDouble(novaLocalizacao.getLatitude()+""))))
				.andExpect(jsonPath("$.dados.localizacao.longitude", 
						equalTo(Double.parseDouble(novaLocalizacao.getLongitude()+""))))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.CONSULTA_REALIZADA.get())));
	}

	@Test
	public void reportarTraicao() throws Exception {
		Rebelde rebeldeToUpdate = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTest.getNome());

		this.mockMvc.perform(patch(baseControllerUrl 
				+ "/" + rebeldeToUpdate.getId() + "/traicao"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(true)))
				.andExpect(jsonPath("$.dados.nome", equalTo(rebeldeToUpdate.getNome())))
				.andExpect(jsonPath("$.dados.reportes", equalTo(rebeldeToUpdate.getReportes() + 1)))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.CONSULTA_REALIZADA.get())));
	}

	@Test
	public void negociarItens() throws Exception {
		Rebelde rebeldeToUpdate1 = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTest.getNome());

		Rebelde rebeldeToUpdate2 = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTest2.getNome());

		Optional<TipoItem> tipoArma = tipoItemRepository.findByNome("Arma");
		Optional<TipoItem> tipoAgua = tipoItemRepository.findByNome("Água");

		Item itemOferta1 = new Item(1, tipoArma.get(), null);
		Item itemOferta2 = new Item(2, tipoAgua.get(), null);

		RequestNegociacao requestNegociacao = new RequestNegociacao();
		requestNegociacao.getOferta1().add(itemOferta1);
		requestNegociacao.getOferta2().add(itemOferta2);

		String jsonObject = json(requestNegociacao);
		System.out.println(jsonObject);
		
		this.mockMvc.perform(patch(baseControllerUrl 
				+ "/" + rebeldeToUpdate1.getId() + "/negociacao/" + rebeldeToUpdate2.getId())
				.contentType(contentType).content(jsonObject))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(true)))
				.andExpect(jsonPath("$.dados.[0].nome", equalTo(rebeldeToUpdate1.getNome())))
				.andExpect(jsonPath("$.dados.[0].inventario[0].tipo.nome", equalTo("Munição")))
				.andExpect(jsonPath("$.dados.[0].inventario[0].quantidade", equalTo(4)))
				.andExpect(jsonPath("$.dados.[0].inventario[1].tipo.nome", equalTo("Arma")))
				.andExpect(jsonPath("$.dados.[0].inventario[1].quantidade", equalTo(3)))
				.andExpect(jsonPath("$.dados.[1].nome", equalTo(rebeldeToUpdate2.getNome())))
				.andExpect(jsonPath("$.dados.[1].inventario[0].tipo.nome", equalTo("Munição")))
				.andExpect(jsonPath("$.dados.[1].inventario[0].quantidade", equalTo(1)))
				.andExpect(jsonPath("$.dados.[1].inventario[1].tipo.nome", equalTo("Água")))
				.andExpect(jsonPath("$.dados.[1].inventario[1].quantidade", equalTo(3)))
				.andExpect(jsonPath("$.dados.[1].inventario[2].tipo.nome", equalTo("Arma")))
				.andExpect(jsonPath("$.dados.[1].inventario[2].quantidade", equalTo(1)))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.CONSULTA_REALIZADA.get())));
	}

	@Test
	public void negociarItensComTraidor() throws Exception {
		Rebelde rebeldeToUpdate1 = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTest.getNome());

		Rebelde rebeldeToUpdate2 = rebeldeRepository
				.findByNomeAllIgnoreCase(this.rebeldeTraidor.getNome());

		Optional<TipoItem> tipoMunicao = tipoItemRepository.findByNome("Munição");
		Optional<TipoItem> tipoComida = tipoItemRepository.findByNome("Comida");

		Item itemOferta1 = new Item(1, tipoMunicao.get(), null);
		Item itemOferta2 = new Item(3, tipoComida.get(), null);

		RequestNegociacao requestNegociacao = new RequestNegociacao();
		requestNegociacao.getOferta1().add(itemOferta1);
		requestNegociacao.getOferta2().add(itemOferta2);

		String jsonObject = json(requestNegociacao);

		this.mockMvc.perform(patch(baseControllerUrl 
				+ "/" + rebeldeToUpdate1.getId() + "/negociacao/" + rebeldeToUpdate2.getId())
				.contentType(contentType).content(jsonObject))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(false)))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.TRAIDOR.get())));
	}

	@Test
	public void obterRelatorio() throws Exception {
		this.mockMvc.perform(get(baseControllerUrl + "/relatorio"))
				.andExpect(content().contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.sucesso", CoreMatchers.is(true)))
				.andExpect(jsonPath("$.dados.porcentagemTraidores", equalTo("33,33")))
				.andExpect(jsonPath("$.dados.porcentagemRebeldes", equalTo("66,67")))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[0].nome", equalTo("Arma")))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[0].quantidadeMediaPorRebelde", CoreMatchers.not(0)))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[1].nome", equalTo("Munição")))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[1].quantidadeMediaPorRebelde", CoreMatchers.not(0)))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[2].nome", equalTo("Água")))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[2].quantidadeMediaPorRebelde", CoreMatchers.not(0)))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[3].nome", equalTo("Comida")))
				.andExpect(jsonPath("$.dados.mediaItensPorTipo[3].quantidadeMediaPorRebelde", CoreMatchers.not(0)))
				.andExpect(jsonPath("$.dados.pontosPerdidosDevidoATraidores", CoreMatchers.not(0)))
				.andExpect(jsonPath("$.mensagem", equalTo(Mensagem.CONSULTA_REALIZADA.get())));
	}

	public void limparTabelas() {
		itemRepository.deleteAllInBatch();
		localizacaoRepository.deleteAllInBatch();
		rebeldeRepository.deleteAllInBatch();
	}

}	
