package com.swr.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swr.api.model.Rebelde;

public interface IRebeldeRepository extends JpaRepository<Rebelde, Long> {
	public Rebelde findByNomeAllIgnoreCase(String nome);

	public Integer countByReportesGreaterThanEqual(Long reportes);
	
	public List<Rebelde> findByReportesGreaterThanEqual(int reportes);
	
	public List<Rebelde> findByReportesLessThan(int reportes);
}
