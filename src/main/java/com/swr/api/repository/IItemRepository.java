package com.swr.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swr.api.model.Item;

@Repository
public interface IItemRepository extends JpaRepository<Item, Long> {

}
