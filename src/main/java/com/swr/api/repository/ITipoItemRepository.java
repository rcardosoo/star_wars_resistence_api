package com.swr.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swr.api.model.TipoItem;

@Repository
public interface ITipoItemRepository extends JpaRepository<TipoItem, Long>{
	
	public Optional<TipoItem> findByNome(String nome);
}
