package com.swr.api.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.swr.api.model.Localizacao;

public interface ILocalizacaoRepository extends JpaRepository<Localizacao, Long> {
	
	@Query(value = "DELETE FROM " + 
			"public.localizacao l " + 
			"WHERE l.rebelde_id = ?1", nativeQuery = true)
	@Modifying
	@Transactional
	public void deleteByRebeldeId(Long rebeldeId);
}
