package com.swr.api.service;

import java.util.HashMap;
import java.util.List;

import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;

public interface IRelatorioService {
	public HashMap<String, Object> gerarRelatorio();

	public List<HashMap<String, Object>> countTotalItensPorTipo(List<TipoItem> tipos, List<Rebelde> totalRebeldes);

	public double porcentagemDeTraidores(List<Rebelde> rebeldes, List<Rebelde> traidores);

	public int calcularPontosPerdidos(List<Rebelde> traidores);
}
