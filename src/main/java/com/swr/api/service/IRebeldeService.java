package com.swr.api.service;

import com.swr.api.model.Localizacao;
import com.swr.api.model.Rebelde;

public interface IRebeldeService {
	
	public Rebelde save(Rebelde rebelde);
	
	public Rebelde atualizarLocalizacao(Long rebeldeId, Localizacao localizacao);
	
	public Rebelde reportarTraicao(Long rebeldeId);
	
	public Boolean isTraidor(Long rebeldeId);
	
	public Boolean existeRecurso(Long rebeldeId);

}
