package com.swr.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swr.api.model.Item;
import com.swr.api.model.Localizacao;
import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;
import com.swr.api.repository.ILocalizacaoRepository;
import com.swr.api.repository.IRebeldeRepository;
import com.swr.api.repository.ITipoItemRepository;
import com.swr.api.service.IRebeldeService;

@Service
public class RebeldeServiceImpl implements IRebeldeService {

	@Autowired
	private IRebeldeRepository rebeldeRepository;

	@Autowired
	private ILocalizacaoRepository localizacaoRepository;
	
	@Autowired
	private ITipoItemRepository tipoItemRepository;

	@Override
	public Rebelde save(Rebelde rebelde) {
		List<Item> inventarioRebelde = rebelde.getInventario();
		Localizacao localizacao = rebelde.getLocalizacao();
		rebelde.setInventario(null);
		rebelde.setLocalizacao(null);
		Rebelde rebeldeToUpdate = rebeldeRepository.save(rebelde);
		rebeldeToUpdate.setLocalizacao(localizacao);
		rebeldeToUpdate.setInventario(inventarioRebelde);
		localizacao.setRebelde(rebeldeToUpdate);
		
		for (Item item : inventarioRebelde) {
			item.setRebelde(rebeldeToUpdate);
			Optional<TipoItem> tipo = tipoItemRepository.findByNome(item.getTipo().getNome());
			if (tipo.isPresent())
				item.setTipo(tipo.get());
			else
				return null;
		}
		
		return rebeldeRepository.save(rebelde);
	}

	@Override
	public Rebelde atualizarLocalizacao(Long rebeldeId, Localizacao localizacao) {
		return rebeldeRepository.findById(rebeldeId).map(rebelde -> {
			localizacaoRepository.deleteByRebeldeId(rebelde.getId());
			rebelde.setLocalizacao(localizacao);
			localizacao.setRebelde(rebelde);
			return rebeldeRepository.save(rebelde);
		}).orElse(null);
	}
	
	@Override
	public Boolean isTraidor(Long rebeldeId) {
		Optional<Rebelde> rebeldeOpt = rebeldeRepository.findById(rebeldeId); 
		return rebeldeOpt.get().getReportes() >= 3;
	} 

	@Override
	public Rebelde reportarTraicao(Long rebeldeId) {
		return rebeldeRepository.findById(rebeldeId).map(rebelde -> {
			rebelde.setReportes(rebelde.getReportes() + 1);
			return rebeldeRepository.save(rebelde);
		}).orElse(null);
	}

	@Override
	public Boolean existeRecurso(Long rebeldeId) {
		Optional<Rebelde> rebeldeOpt1 = rebeldeRepository.findById(rebeldeId);
		return rebeldeOpt1.isPresent();
	}

}
