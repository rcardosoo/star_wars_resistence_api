package com.swr.api.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swr.api.model.Item;
import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;
import com.swr.api.repository.IRebeldeRepository;
import com.swr.api.repository.ITipoItemRepository;
import com.swr.api.service.IRelatorioService;

@Service
public class RelatorioServiceImpl implements IRelatorioService {
	
	@Autowired
	private IRebeldeRepository rebeldeRepository;
	
	@Autowired
	private ITipoItemRepository tipoItemRepository;
	
	@Override
	public double porcentagemDeTraidores(List<Rebelde> rebeldes, List<Rebelde> traidores) {
		double totalRebeldes = rebeldes.size();
		double totalTraidores = traidores.size();
		if (totalTraidores == 0)
			return 0;
		
		return (100 * totalTraidores) / totalRebeldes;
	}
	
	@Override
	public List<HashMap<String, Object>> countTotalItensPorTipo(List<TipoItem> tipos, List<Rebelde> totalRebeldes) {
		List<HashMap<String, Object>> lista = new ArrayList<HashMap<String, Object>>();
		
		for(TipoItem tipo : tipos) {
			HashMap<String, Object> mediaItemPorTipo = new HashMap<>();
			int quantidadeRebeldes=0;
			int quantidadeAcumulada=0;
			for (Rebelde rebelde : totalRebeldes) {
				for (Item item : rebelde.getInventario()) {
					if (item.getTipo().getNome().equals(tipo.getNome())) {
						quantidadeRebeldes++;
						quantidadeAcumulada = quantidadeAcumulada + item.getQuantidade();
					}
				}
			}
			mediaItemPorTipo.put("nome", tipo.getNome());
			if (quantidadeRebeldes != 0)
				mediaItemPorTipo.put("quantidadeMediaPorRebelde", quantidadeAcumulada/quantidadeRebeldes);
			else
				mediaItemPorTipo.put("quantidadeMediaPorRebelde", 0);
			
			lista.add(mediaItemPorTipo);
		}
		
		return lista;
	}
	
	@Override
	public int calcularPontosPerdidos(List<Rebelde> traidores) {
		int pontosPerdidos=0;

		for (Rebelde rebelde : traidores) {
			for (Item item : rebelde.getInventario()) {
				pontosPerdidos += item.getTipo().getPontos() * item.getQuantidade();
			}
		}
		return pontosPerdidos;
	}

	@Override
	public HashMap<String, Object> gerarRelatorio() {
		HashMap<String, Object> relatorio = new HashMap<>();
		
		List<Rebelde> totalRebeldes = rebeldeRepository.findAll();
		List<Rebelde> totalTraidores = rebeldeRepository.findByReportesGreaterThanEqual(3);
		
		DecimalFormat dec = new DecimalFormat("#0.00");
		double porcentagemTraidores = this.porcentagemDeTraidores(totalRebeldes, totalTraidores);
		
		relatorio.put("porcentagemTraidores", dec.format(porcentagemTraidores));
		relatorio.put("porcentagemRebeldes", dec.format(100 - porcentagemTraidores));
		
		List<TipoItem> tipos = tipoItemRepository.findAll();
		
		List<Rebelde> rebeldesNaoTraidores = rebeldeRepository.findByReportesLessThan(3);
		
		//Traidores não devem ser considerados no relatório
		if (!rebeldesNaoTraidores.isEmpty())
			relatorio.put("mediaItensPorTipo", this.countTotalItensPorTipo(tipos, totalRebeldes));
		else
			relatorio.put("mediaItensPorTipo", new ArrayList<>());
		
		relatorio.put("pontosPerdidosDevidoATraidores", this.calcularPontosPerdidos(totalTraidores));
		return relatorio;
	}
}
