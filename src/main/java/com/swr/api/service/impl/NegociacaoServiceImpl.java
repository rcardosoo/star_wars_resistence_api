package com.swr.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swr.api.model.Item;
import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;
import com.swr.api.repository.IRebeldeRepository;
import com.swr.api.repository.ITipoItemRepository;
import com.swr.api.service.INegociacaoService;
import com.swr.api.util.RequestNegociacao;

@Service
public class NegociacaoServiceImpl implements INegociacaoService {
	
	@Autowired
	private IRebeldeRepository rebeldeRepository;
	
	@Autowired
	private ITipoItemRepository tipoItemRepository;
	
	@Override
	public List<Rebelde> realizarNegociacao(Long rebeldeId1, Long rebeldeId2,
			RequestNegociacao ofertas) {
		
		Optional<Rebelde> rebeldeOpt1 = rebeldeRepository.findById(rebeldeId1);
		Optional<Rebelde> rebeldeOpt2 = rebeldeRepository.findById(rebeldeId2);
		
		Rebelde rebelde1 = removerOfertaRebelde(ofertas.getOferta1(), rebeldeOpt1.get());
		Rebelde rebelde2 = removerOfertaRebelde(ofertas.getOferta2(), rebeldeOpt2.get());
		
		rebelde1 = adicionarOfertaRebelde(ofertas.getOferta2(), rebelde1);
		rebelde2 = adicionarOfertaRebelde(ofertas.getOferta1(), rebelde2);
		
		List<Rebelde> listaRebeldes = new ArrayList<>();
		listaRebeldes.add(rebeldeRepository.save(rebelde1));
		listaRebeldes.add(rebeldeRepository.save(rebelde2));
		
		return listaRebeldes;	
	}

	@Override
	public boolean validarOfertas(RequestNegociacao ofertas) {
		if (ofertas.getOferta1().isEmpty() || ofertas.getOferta2().isEmpty())
			return false;
		
		if (calcularPontos(ofertas.getOferta1()) != calcularPontos(ofertas.getOferta2()))
			return false;
		
		return true;
	}
	
	@Override
	public int calcularPontos(List<Item> oferta) {
		int pontos = 0;
		for(Item item : oferta) {
			pontos += (item.getTipo().getPontos()) * item.getQuantidade();
		}
		return pontos;
	}

	@Override
	public Rebelde removerOfertaRebelde(List<Item> oferta, Rebelde rebelde) {
		for (Item item : oferta) {
			for (Item itemRebelde : rebelde.getInventario()) {
				if (item.getTipo().getNome().equals(itemRebelde.getTipo().getNome())) {
					itemRebelde.setQuantidade(itemRebelde.getQuantidade() - item.getQuantidade());
					if (itemRebelde.getQuantidade() <= 0) {
						rebelde.getInventario().remove(itemRebelde);
						itemRebelde.setRebelde(null);
					}
				}
			}
		}
		return rebelde;
	}

	@Override
	public Rebelde adicionarOfertaRebelde(List<Item> oferta, Rebelde rebelde) {
		List<Item> listaAux = new ArrayList<>();
		for (Item item : oferta) {
			for (Item itemRebelde : rebelde.getInventario()) {
				if (item.getTipo().getNome().equals(itemRebelde.getTipo().getNome())) {
					itemRebelde.setQuantidade(itemRebelde.getQuantidade() + item.getQuantidade());
				} else {
					Optional<TipoItem> tipoTemp = tipoItemRepository.findByNome(item.getTipo().getNome());
					if (tipoTemp.isPresent()) {
						listaAux.add(new Item(1, tipoTemp.get(), rebelde));	
					}
				}
			}
		}
		rebelde.getInventario().addAll(listaAux);
		return rebelde;
	}
	
}
