package com.swr.api.service;

import java.util.List;

import com.swr.api.model.Item;
import com.swr.api.model.Rebelde;
import com.swr.api.util.RequestNegociacao;

public interface INegociacaoService {
	
	public List<Rebelde> realizarNegociacao(Long rebeldeId1, Long rebeldeId2, RequestNegociacao ofertas);

	public boolean validarOfertas(RequestNegociacao ofertas);
	
	public int calcularPontos(List<Item> oferta);
	
	public Rebelde removerOfertaRebelde(List<Item> oferta, Rebelde rebelde);
	
	public Rebelde adicionarOfertaRebelde(List<Item> oferta, Rebelde rebelde);
	
}
