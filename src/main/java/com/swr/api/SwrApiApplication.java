package com.swr.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.swr.api.seeder.TipoItemSeeder;

@SpringBootApplication
public class SwrApiApplication implements CommandLineRunner {
	
	@Autowired
	private TipoItemSeeder tipoItemSeeder;
	
	public static void main(String[] args) {
		SpringApplication.run(SwrApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		tipoItemSeeder.runSeeder();
	}
}
