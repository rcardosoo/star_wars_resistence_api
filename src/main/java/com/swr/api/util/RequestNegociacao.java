package com.swr.api.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swr.api.model.Item;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestNegociacao implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Item> oferta1 = new ArrayList<>();

	private List<Item> oferta2 = new ArrayList<>();
	

	public RequestNegociacao() {
		super();
	}

	public List<Item> getOferta1() {
		return oferta1;
	}

	public void setOferta1(List<Item> oferta1) {
		this.oferta1 = oferta1;
	}

	public List<Item> getOferta2() {
		return oferta2;
	}

	public void setOferta2(List<Item> oferta2) {
		this.oferta2 = oferta2;
	}
	
}
