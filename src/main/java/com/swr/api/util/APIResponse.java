package com.swr.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SuppressWarnings({"rawtypes", "unchecked"})
public class APIResponse {
	static final String STATUS_KEY 		= "sucesso";
	static final String DADOS_KEY 		= "dados";
	static final String MENSAGEM_KEY 	= "mensagem";
	private Map<String, Object> map = new HashMap<>();
	
	public APIResponse() {
		//
	}
	
	public ResponseEntity create(Object dados) {
		this.map.put(STATUS_KEY, true);
		this.map.put(MENSAGEM_KEY, Mensagem.RECURSO_CRIADO.get());
		this.map.put(DADOS_KEY, dados);
		return new ResponseEntity(this.map, HttpStatus.CREATED);
	}
	
	public ResponseEntity success(Object dados) {
		this.map.put(STATUS_KEY, true);
		this.map.put(MENSAGEM_KEY, Mensagem.CONSULTA_REALIZADA.get());
		this.map.put(DADOS_KEY, dados);
		return new ResponseEntity(this.map, HttpStatus.OK);
	}
	
	public ResponseEntity notFound() {
		this.map.put(STATUS_KEY, false);
		this.map.put(MENSAGEM_KEY, Mensagem.RECURSO_NAO_ENCONTRADO.get());
		this.map.put(DADOS_KEY, new ArrayList<>());
		return new ResponseEntity(this.map, HttpStatus.NOT_FOUND);
	}
	
	public ResponseEntity badRequest() {
		this.map.put(STATUS_KEY, false);
		this.map.put(MENSAGEM_KEY, Mensagem.REQUISICAO_MAL_FORMADA.get());
		this.map.put(DADOS_KEY, new ArrayList<>());
		return new ResponseEntity(this.map, HttpStatus.BAD_REQUEST);
	}
	
	public ResponseEntity unauthorized() {
		this.map.put(STATUS_KEY, false);
		this.map.put(MENSAGEM_KEY, Mensagem.TRAIDOR.get());
		this.map.put(DADOS_KEY, new ArrayList<>());
		return new ResponseEntity(this.map, HttpStatus.UNAUTHORIZED);
	}
}
