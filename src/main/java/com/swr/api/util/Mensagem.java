package com.swr.api.util;

import java.util.HashMap;
import java.util.Map;

public enum Mensagem {
	RECURSO_CRIADO("O Recurso foi cadastrado com sucesso"),
	CONSULTA_REALIZADA("Operação realizada com sucesso"),
	RECURSO_NAO_ENCONTRADO("O recurso não foi encontrado"),
	REQUISICAO_MAL_FORMADA("A requisição não está correta"),
	TRAIDOR("Um traidor não pode realizar negociações");
    private final String value;
    
    Mensagem(String value) {
        this.value = value;
    }
    
    public String get() {
    	return this.value;
    }
    
    public Map<String, String> show() {
        Map<String, String> map = new HashMap<>();
        map.put("Mensagem", this.value);
        return map;
    }
}
