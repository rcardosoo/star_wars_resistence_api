package com.swr.api.seeder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.swr.api.model.TipoItem;
import com.swr.api.repository.ITipoItemRepository;

@Component
public class TipoItemSeeder {

	@Autowired
	private ITipoItemRepository tipoItemRepository;
	
	public void runSeeder() {
		if(tipoItemRepository.findAll().isEmpty()) {
			tipoItemRepository.save(new TipoItem("Arma", 4));
			tipoItemRepository.save(new TipoItem("Munição", 3));
			tipoItemRepository.save(new TipoItem("Água", 2));
			tipoItemRepository.save(new TipoItem("Comida", 1));
		}
	}
}
