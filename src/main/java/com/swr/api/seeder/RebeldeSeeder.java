package com.swr.api.seeder;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.swr.api.model.Item;
import com.swr.api.model.Localizacao;
import com.swr.api.model.Rebelde;
import com.swr.api.model.TipoItem;
import com.swr.api.repository.IRebeldeRepository;
import com.swr.api.repository.ITipoItemRepository;

@Component
public class RebeldeSeeder {

	@Autowired
	private IRebeldeRepository rebeldeRepository;

	@Autowired
	private ITipoItemRepository tipoItemRepository;

	public Rebelde inserirRebeldeTeste1() {
		Localizacao localizacaoTest = new Localizacao("Vega", new BigDecimal("88.98"), new BigDecimal("-75.44"));
				
		Rebelde rebeldeTest = new Rebelde("Rafael", 23, "Masculino");
		localizacaoTest.setRebelde(rebeldeTest);
		rebeldeTest.setLocalizacao(localizacaoTest);
		
		Optional<TipoItem> tipoArma = tipoItemRepository.findByNome("Arma");
		Optional<TipoItem> tipoComida = tipoItemRepository.findByNome("Comida");
		
		Item itemTest = new Item(1, tipoArma.get(), rebeldeTest);
		Item itemTest2 = new Item(1, tipoComida.get(), rebeldeTest);
		
		rebeldeTest.getInventario().add(itemTest);
		rebeldeTest.getInventario().add(itemTest2);
		
		return rebeldeTest;
	}

	public Rebelde inserirRebeldeTeste2() {
		Localizacao localizacaoTest = new Localizacao("Kepler-186f", new BigDecimal("-18.98"), new BigDecimal("40.89"));		
		
		Rebelde rebeldeTest = rebeldeRepository.save(new Rebelde("Jose", 40, "Masculino"));

		localizacaoTest.setRebelde(rebeldeTest);
		rebeldeTest.setLocalizacao(localizacaoTest);
		
		Optional<TipoItem> tipoMunicao = tipoItemRepository.findByNome("Munição");
		Optional<TipoItem> tipoAgua = tipoItemRepository.findByNome("Água");
		
		Item itemTest = new Item(1, tipoMunicao.get(), rebeldeTest);
		Item itemTest2 = new Item(5, tipoAgua.get(), rebeldeTest);
		
		rebeldeTest.getInventario().add(itemTest);
		rebeldeTest.getInventario().add(itemTest2);

		return rebeldeRepository.save(rebeldeTest);
	}
	
	public Rebelde inserirRebeldeTeste3() {
		Localizacao localizacaoTest = new Localizacao("Gargantua", new BigDecimal("99.88"), new BigDecimal("88.99"));	
		
		Rebelde rebeldeTest = rebeldeRepository.save(new Rebelde("John", 12, "Masculino"));
		localizacaoTest.setRebelde(rebeldeTest);
		rebeldeTest.setLocalizacao(localizacaoTest);
		
		Optional<TipoItem> tipoMunicao = tipoItemRepository.findByNome("Munição");
		Optional<TipoItem> tipoArma = tipoItemRepository.findByNome("Arma");
		
		Item itemTest = new Item(4, tipoMunicao.get(), rebeldeTest);
		Item itemTest2 = new Item(4, tipoArma.get(), rebeldeTest);
		
		rebeldeTest.getInventario().add(itemTest);
		rebeldeTest.getInventario().add(itemTest2);

		return rebeldeRepository.save(rebeldeTest);
	}
	
	public Rebelde inserirRebeldeTesteTraidor() {
		Localizacao localizacaoTest = new Localizacao("Titã", new BigDecimal("50.28"), new BigDecimal("30.60"));
		
		Rebelde rebeldeTest = rebeldeRepository.save(new Rebelde("Judas", 30, "Masculino"));
		localizacaoTest.setRebelde(rebeldeTest);
		rebeldeTest.setLocalizacao(localizacaoTest);
		
		Optional<TipoItem> tipoComida = tipoItemRepository.findByNome("Comida");
		Optional<TipoItem> tipoAgua = tipoItemRepository.findByNome("Água");
		
		Item itemTest = new Item(5, tipoComida.get(), rebeldeTest);
		Item itemTest2 = new Item(7, tipoAgua.get(), rebeldeTest);
		
		rebeldeTest.getInventario().add(itemTest);
		rebeldeTest.getInventario().add(itemTest2);
		
		rebeldeTest.setReportes(3);
		return rebeldeRepository.save(rebeldeTest);
	}
}
