package com.swr.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swr.api.model.Localizacao;
import com.swr.api.model.Rebelde;
import com.swr.api.service.INegociacaoService;
import com.swr.api.service.IRebeldeService;
import com.swr.api.service.IRelatorioService;
import com.swr.api.util.APIResponse;
import com.swr.api.util.RequestNegociacao;

@RestController
@RequestMapping("/api/rebeldes")
@SuppressWarnings({"rawtypes"})
public class RebeldeController {

	@Autowired
	IRebeldeService rebeldeService;
	
	@Autowired
	IRelatorioService relatorioService;
	
	@Autowired
	INegociacaoService negociacaoService;

	@PostMapping()
	public ResponseEntity cadastrarRebelde(@Valid @RequestBody Rebelde rebelde) {
		Rebelde criado = rebeldeService.save(rebelde);
		return (criado == null) ? new APIResponse().notFound() : new APIResponse().create(criado);	
	}
	
	@PatchMapping(value = "/{rebeldeId}")
	public ResponseEntity atualizarLocalizacao(@PathVariable Long rebeldeId, 
			@Valid @RequestBody Localizacao localizacao) {
		
		Rebelde atualizado = rebeldeService.atualizarLocalizacao(rebeldeId, localizacao);
		return (atualizado == null) ? new APIResponse().notFound() : new APIResponse().success(atualizado);	
	}
	
	@PatchMapping(value = "/{rebeldeId}/traicao")
	public ResponseEntity reportarTraicao(@PathVariable Long rebeldeId) {
		Rebelde atualizado = rebeldeService.reportarTraicao(rebeldeId);
		return (atualizado == null) ? new APIResponse().notFound() : new APIResponse().success(atualizado);	
	}
	
	@PatchMapping(value = "/{rebeldeId1}/negociacao/{rebeldeId2}")
	public ResponseEntity realizarNegociacao(@PathVariable Long rebeldeId1, 
			@PathVariable Long rebeldeId2, @Valid @RequestBody RequestNegociacao ofertas) {
		
		if (!rebeldeService.existeRecurso(rebeldeId1) || !rebeldeService.existeRecurso(rebeldeId2)) {
			return new APIResponse().notFound();
		}
		
		if (!negociacaoService.validarOfertas(ofertas)) {
			return new APIResponse().badRequest();
		}
		
		if (rebeldeService.isTraidor(rebeldeId1) || rebeldeService.isTraidor(rebeldeId2)) {
			return new APIResponse().unauthorized();
		}
		
		List<Rebelde> atualizados = negociacaoService.realizarNegociacao(rebeldeId1, rebeldeId2, ofertas);
		return new APIResponse().success(atualizados);
	}
	
	@GetMapping(value = "/relatorio")
	public ResponseEntity gerarRelatorio() {
		return new APIResponse().success(relatorioService.gerarRelatorio());
	}
}
